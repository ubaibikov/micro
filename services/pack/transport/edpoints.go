package transport

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"micro/services/pack"
)

type Endpoints struct {
	Create 	 endpoint.Endpoint
	GetByID  endpoint.Endpoint
}

func MakeEndpoints(s pack.Service) Endpoints {
	return Endpoints{
		Create: 	makeCreateEndpoint(s),
		GetByID:	makeGetByIDEndpoint(s),
	}
}

func makeCreateEndpoint(s pack.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(CreateRequest) // приводим к типу
		id, err := s.CreatePack(ctx, req.Pack)

		return CreateResponse{id,err}, nil
	}
}


func makeGetByIDEndpoint(s pack.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetByIDRequest) // приводим к типу
		packByID, err := s.GetPackByID(ctx, req.ID)

		return GetByIDResponse{packByID, err}, nil
	}
}