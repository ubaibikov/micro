package transport

import "micro/services/pack"

// create ----
type CreateRequest struct {
	Pack 	pack.Pack
}

type CreateResponse struct {
	ID 		int64 `json:"id"`
	Err		error `json:"error,omitempty"`
}

// get by id --
type GetByIDRequest struct {
	ID 		int64
}

type GetByIDResponse struct {
	Pack 	*pack.Pack `json:"pack"`
	Err		error `json:"error,omitempty"`
}
