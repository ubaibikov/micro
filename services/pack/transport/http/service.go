package http

import (
	"context"
	"encoding/json"
	"errors"
	khttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"log"
	"micro/services/pack"
	"micro/services/pack/transport"
	"net/http"
)

var (
	ErrBadRouting 	= errors.New("bad routing")
)

func NewService(
	e transport.Endpoints,
	options []khttp.ServerOption,
	logger *log.Logger,
) http.Handler {
	var (
		r  			 = mux.NewRouter()
		errorEncoder = khttp.ServerErrorEncoder(encodeErrorResponse)
	)


	options = append(options, errorEncoder)

	r.Methods("POST").Path("/packs").Handler(khttp.NewServer(
		e.Create,
		decodeCreateRequest,
		encodeResponse,
		options...,
	))

	return r

}

func decodeCreateRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	var req transport.CreateRequest
	if e := json.NewDecoder(r.Body).Decode(&req.Pack); e != nil {
		return nil, e
	}
	return req, nil
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		encodeErrorResponse(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

type errorer interface {
	error() error
}

func encodeErrorResponse(_ context.Context, err error, w http.ResponseWriter) {
	if err == nil {
		panic("encodeError with nil error")
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(codeFrom(err))
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}

func codeFrom(err error) int {
	switch err {
	case pack.ErrRatioNotFound:
		return http.StatusBadRequest
	default:
		return http.StatusInternalServerError
	}
}