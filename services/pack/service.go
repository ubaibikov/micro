package pack

import (
	"context"
	"errors"
)

var (
	// ErrRatioNotFound - ratio not found error
	ErrRatioNotFound = errors.New("ration not found")

	// ErrCmdRepo - cmd repo unble command error
	ErrCmdRepo 		 = errors.New("unable command")

	// ErrQuery - err query unable query error
	ErrQuery 		 = errors.New("unable query repo")
)

// Service - pack service engine
type Service interface {
	CreatePack(ctx context.Context, r Pack) (int64, error)
	GetPackByID(ctx context.Context, id int64) (*Pack, error)
}















