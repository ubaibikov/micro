// package for using call to postgres db
package pq

import (
	"context"
	"database/sql"
	"log"
	"micro/services/pack"
)


type repo struct{
	db 	*sql.DB
	l 	*log.Logger
}

// New return concrete repo backend by Postgres DB
func New(db *sql.DB, l *log.Logger) pack.Repository {
	return &repo{db: db, l: l}
}

// Create pack pack
func (rp repo) CreatePack(ctx context.Context, r pack.Pack) (int64, error) {
	return 0, nil
}
// Get pack pack by id
func (rp repo) GetPackByID(ctx context.Context, id int64) (*pack.Pack, error) {
	return nil, nil
}