package service

import (
	"context"
	"log"
	"micro/services/pack"
)

type service struct {
	rp 	pack.Repository
	l	*log.Logger
}

func New(rp pack.Repository, l *log.Logger ) pack.Service {
	return &service{
		rp: rp,
		l:  l,
	}
}


func (s *service) CreatePack(ctx context.Context, pack pack.Pack) (int64, error) {
	return s.rp.CreatePack(ctx, pack)
}

func (s *service) GetPackByID(ctx context.Context, id int64) (*pack.Pack, error) {
	return s.rp.GetPackByID(ctx, id)
}