package pack

import "context"

type Pack struct {
	ID 			int64 	`json:"id"`
	Code 		string	`json:"code"`
	TaxPack 	string	`json:"tax_pack"`
	Uniq		int64	`json:"uniq"`
	CreatedBy	string	`json:"created_by"`
}


type Repository interface {
	CreatePack(ctx context.Context, r Pack) (int64, error)
	GetPackByID(ctx context.Context, id int64) (*Pack, error)
}